"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const foodItemSchema = new Schema(
  {
    itemName: String,
    status: String,
    reason: String
  },
  {
    timestamps: false
  }
);

module.exports = foodItemSchema;
