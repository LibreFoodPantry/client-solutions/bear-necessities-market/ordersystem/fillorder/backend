"use strict";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var foodItemSchema = require("./foodItem");

var orderSchema = new Schema(
  {
    details: {
      foodItems: [foodItemSchema],
      dateFilled: Number,
      dateOrdered: { type: Number, default: new Date().getTime() }
    }
  },
  {
    timestamps: false
  }
);

const Order = mongoose.model("Order", orderSchema);
module.exports = Order;
