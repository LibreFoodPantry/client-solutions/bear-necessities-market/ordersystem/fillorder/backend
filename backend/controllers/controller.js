("use strict");

var mongoose = require("mongoose"),
  Order = mongoose.model("Order");

exports.listAllOrders = function (req, res) {
  Order.find({}, function (err, order) {
    if (err) res.send(err);
    else res.json(order);
  });
};

exports.getOrderByID = function (req, res) {
  Order.find({ _id: req.params.id }, function (err, order) {
    if (err) res.send({ message: "error" });
    else res.json(order);
  });
};

exports.createOrder = function (req, res) {
  var newOrder = new Order(req.body);
  newOrder.save(function (err, order) {
    if (err) res.send(err);
    else {
      console.log(order);
      res.send(order);
    }
  });
};

exports.deleteOrder = function (req, res) {
  Order.deleteOne({ _id: req.params.id }, function (err, order) {
    if (err) res.send();
    else res.send({
      message: "Order has been deleted",
      orderObject: order
    });
  });
};

// https://docs.mongodb.com/manual/reference/operator/query-comparison/
exports.allOrdersAfterUnixtime = function (req, res) {
  Order.find({ "details.dateOrdered": { $gte: req.params.ts } }, function (err, order) {
    if (err) res.send();
    else res.json({ message: "dateOrdered: " + req.params.ts, order });
  });
};
