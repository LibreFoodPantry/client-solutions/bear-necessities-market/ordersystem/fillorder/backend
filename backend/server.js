require("dotenv").config();
var express = require("express"),
  app = express(),
  port = process.env.PORT || 3001,
  mongoose = require("mongoose"),
  Order = require("./db/models/Order"),
  bodyParser = require("body-parser");

mongoose.Promise = global.Promise;

mongoose.set("useUnifiedTopology", true);
mongoose.connect(process.env.FO_MONGODB_TEST, { useNewUrlParser: true });

// app.use(bodyParser);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const routes = require("./routes/routes");
routes(app);

app.use(function(req, res) {
  res.status(404).send({ url: req.originalUrl + " not found" });
});

app.listen(port);

console.log("RESTful API toy project: " + port);
