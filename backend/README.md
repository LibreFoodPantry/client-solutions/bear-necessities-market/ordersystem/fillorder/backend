## RESTful API

### End Points
Utilizing order `id` to `GET`, `POST`, and `DELETE` a single order <br>
 - `/api/fill-order/id?id={id #}` <br>

Utilizing `unixtimestamp` to get all orders after a given date <br>
 - `/api/fill-order/timestamp?ts={unix time}` <br>

Utilizing `order` as the keyword to return a list of all orders <br>
 - `/api/fill-order/order` <br>

## Technology Stack

- NodeJS
- express
- nodemon (dev)
- MongoDB
- Mongoose
- dotenv

## Commands:

`npm run start` is used to start the server.

## Technology Description

### NodeJS

### express

### nodemon (dev)

### MongoDB

### Mongoose

Mongoose is an ODM which gives the ablity to define JSON object schema