module.exports = function (app) {
  var control = require("../controllers/controller");

  require("dotenv").config();

  app
    .route(process.env.FO_API_URL + "order")
    .get(control.listAllOrders)
    .post(control.createOrder);

  app
    .route(process.env.FO_API_URL + "timestamp/:ts")
    .get(control.allOrdersAfterUnixtime);

  app
    .route(process.env.FO_API_URL + "order/id/:id")
    .get(control.getOrderByID)
    .delete(control.deleteOrder);
};
