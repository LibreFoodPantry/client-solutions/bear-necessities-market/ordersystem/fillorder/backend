# .gitattributes

This file helps git resolve line endings in files across developer with
different operating systems.

The project comes with a default .gitattributes for file types found in many
different projects.  However, this file needs to be updated with file types
based on the programming langauges used in the project.

Good templates for .gitattributes can be found at
https://github.com/alexkaratarakis/gitattributes.  The initial .gitattributes
file that came with project came from Common.gitattributes from that
repository. That repository also has a link to a generator that will combine
templates into a single file that you can copy and paste to this project's
.gitattributes file.  This is the recommended way of updating this file.
